
<hr>

<!-- Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">

            <p>
              Copyright &copy; 2016-<?php echo date('Y'); ?> the MustangX Project.
            </p>

            <p>
              <ul class="soc">
                <li><a class="soc-twitter" href="#"></a></li>
                <li><a class="soc-facebook" href="#"></a></li>
                <li><a class="soc-googleplus" href="#"></a></li>
                <li><a class="soc-pinterest" href="#"></a></li>
                <li><a class="soc-linkedin" href="#"></a></li>
                <li><a class="soc-rss soc-icon-last" href="#"></a></li>
              </ul>
            </p>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</footer>

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
