<?php include('includes/header.php'); ?>
<?php include('../includes/database.php'); ?>

<?php
if(isset($_POST['submit'])) {

  $form_cat_title = $_POST['form_cat_title'];

  $query = "INSERT INTO categories(cat_title) VALUES('{$form_cat_title}')";

  $insert_category = mysqli_query($connection, $query);

    if(!$insert_category) {

      die("Query Failed" . mysqli_error());

    }

}
?>


<body>

    <div id="wrapper">

        <!-- Navigation -->
<?php include("includes/nav.php"); ?>



        <div id="page-wrapper">

            <div class="container-fluid">

              <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard <small>Statistics Overview</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Add Category
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                <div class="col-md-4">
                  <form action="" method="post">

                    <div class="form-group">
                      <input type="text" name="form_cat_title" class="form-control">
                    </div>

                    <div class="form-group">
                      <input type="submit" name="submit" class="btn btn-primary">
                    </div>

                  </form>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include("includes/footer.php"); ?>
